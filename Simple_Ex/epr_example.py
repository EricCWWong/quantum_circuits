import qiskit

qreg = qiskit.QuantumRegister(2, 'qreg')

creg = qiskit.ClassicalRegister(2, 'creg')

circ = qiskit.QuantumCircuit(qreg, creg)

circ.x(qreg[1])

circ.h(qreg[0])

circ.cx(qreg[0], qreg[1])

circ.measure(qreg, creg)

print(circ.draw())

backend = qiskit.BasicAer.get_backend("qasm_simulator")

job = qiskit.execute(circ, backend)

result = job.result()

print(result.get_counts())