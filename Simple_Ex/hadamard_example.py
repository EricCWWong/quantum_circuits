import qiskit

qreg = qiskit.QuantumRegister(1, name= 'qreg')

creg = qiskit.ClassicalRegister(1, name= 'creg')

circ = qiskit.QuantumCircuit(qreg, creg)

circ.h(qreg[0])

circ.measure(qreg, creg)

print(circ.draw())

backend = qiskit.BasicAer.get_backend("qasm_simulator")

job = qiskit.execute(circ, backend)

result = job.result()

print(result.get_counts())